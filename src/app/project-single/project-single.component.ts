import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from '../interfaces/project.model';
import { ProjectService } from '../services/project.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup } from '@angular/forms';
import { take, tap } from 'rxjs';

@Component({
  selector: 'app-project-single',
  templateUrl: './project-single.component.html',
  styleUrls: ['./project-single.component.scss']
})
export class ProjectSingleComponent implements OnInit {
  project: Project = {
    name: '', description: '', creationDate: '',
    fundingGoal: 0, id: 0, media: '', backers: 0,
    endDate: new Date(), catchPhrase: '', fundRaised: 0
  };
  projectForm!: FormGroup;
  submitted = false;
  loading = false;
  field = '';
  upload?: File;

  /// EDITOR
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  /// END EDITOR

  constructor(
    private projectService: ProjectService, private route: ActivatedRoute, private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getOne(this.route.snapshot.paramMap.get('id'));
    this.projectForm = this.formProject();
    console.log(this.project);

  }

  getOne(id: any): void {
    this.projectService.find(id).pipe(
      tap((data: any) => {
        this.project = data;
        this.initializeUpdateForm(this.project);
        console.log(data);
      }),
      take(1)
    ).subscribe();
  }

  initializeUpdateForm(proj: Project) {
    this.projectForm!.setValue({
      name: proj.name,
      description: proj.description,
      media: proj.media,
      backers: proj.backers,
      fundingGoal: proj.fundingGoal,
      creationDate: proj.creationDate,
      endDate: proj.endDate,
      levels: proj.levels
    })
  }

  formProject(): FormGroup {
    return this.formBuilder.group(
      {
        name: null,
        description: null,
        media: null,
        backers: null,
        fundingGoal: null,
        creationDate: null,
        endDate: null,
        levels: null
      },
    );
  }

  submitProjectForm() {
    this.submitted = true;
    this.loading = true;
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

    if (this.project.id) {
      console.log("projectForm values", this.projectForm.value);
      console.log(this.projectForm.value['description']);
      console.log(typeof this.projectForm.value['description']);

      this.projectService.update(this.project.id, this.projectForm.value).pipe(
        tap(
          () => {
            this.loading = false;
            this.router.navigate(['../board/projects']);
            console.log("Le projet a bien été modifiée.");
          },
        )
      ).subscribe();
    } else {
      console.log(JSON.stringify(this.projectForm!.value, null, 2));
    }
  }

  deleteProject(id: number): void {
    this.projectService.delete(id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/projects']);
        });
  }

  selectFile(event: any): void {
    this.upload = event.target.files[0];
    if (this.upload) {
      if (this.upload) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          console.log("e target result : ", e.target.result);
          const base64String = e.target.result
            .replace('data:', '')
            .replace(/^.+,/, '');
          this.project.media = e.target.result;
          this.projectForm.value['media'] = base64String;
        };
        reader.readAsDataURL(this.upload);
      }
    }
  }
}
