export interface Project {
    id: number;
    name: string;
    description: string;
    media: string;
    backers: number;
    fundingGoal: number;
    creationDate: any;
    endDate: Date;
    levels?: string;
    catchPhrase: string;
    fundRaised: number;
}