import { Component, OnInit, ViewChild } from '@angular/core';
import { Project } from '../interfaces/project.model';
import { ProjectService } from '../services/project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  projects: Project[] = [];

  constructor(private projectService: ProjectService) {
  }

  ngOnInit() {
    console.log(this.projectService.findAll());
    this.projectService.findAll()
      .subscribe(
        data => {
          this.projects = data;
        });
  }

  // applyFilter(filterValue: string) {
  //   this.projects.filter = filterValue.trim().toLowerCase();
  // }


}
