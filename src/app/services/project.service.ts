import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../interfaces/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private apiUrl = '/api/projects';

  constructor(private http: HttpClient) { }

  findAll(): Observable<Project[]> {
    return this.http.get<Project[]>(this.apiUrl);
  }

  find(id: number): Observable<Project> {
    return this.http.get<Project>(this.apiUrl + '/' + id);
  }

  create(project: Project): Observable<Project> {
    project.creationDate = project.creationDate.format('YYYY-MM-DD');
    return this.http.post<Project>(this.apiUrl, project);
  }

  update(id: number, data: Project): Observable<any> {
    return this.http.put(`${this.apiUrl}/${id}`, data);
  }

  delete(id: number): Observable<Project> {
    return this.http.delete<Project>(this.apiUrl + '/' + id);
  }

}
