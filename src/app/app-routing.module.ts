import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardComponent } from './board/board.component';
import { AdminGuard } from './core/helpers/admin.guard';
import { LoginComponent } from './core/login/login.component';
import { RegisterComponent } from './core/register/register.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectSingleComponent } from './project-single/project-single.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  {
    path: 'board', component: BoardComponent, canActivate: [AdminGuard], children: [
      { path: 'projects', component: ProjectListComponent },
      { path: 'project/:id', component: ProjectSingleComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
