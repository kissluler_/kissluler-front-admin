import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Injectable({
    providedIn: 'root',
})
export class AdminGuard implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const authUser = this.authService.state.user;
        const authUserAuthorities = this.authService.state.authorities;
        if (authUser && authUserAuthorities && authUserAuthorities == "ROLE_ADMIN") {
            return true;
        }
        this.router.navigate(["/"]);
        return false;
    }

}