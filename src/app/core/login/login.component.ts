import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  email: string = '';
  password: string = '';
  hasError = false;
  hidePassword = true;
  loading = false;
  errorMessage: string = '';

  @Output() isLoginFailed: EventEmitter<any> = new EventEmitter();

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private router: Router
  ) {
    if (this.authService.state.user) {
      this.router.navigate(['/board']);
    }
  }

  login() {
    this.hasError = false;
    this.loading = true;
    this.authService.login(this.email, this.password).subscribe({
      next: (d: any) => {
        if (d.authorities[0].authority != "ROLE_ADMIN") {
          this.hasError = true;
          this.loading = false;
          this.errorMessage = "Vous n'avez pas les accès nécessaire.";
          localStorage.clear();
        }
        this.isLoginFailed.emit(false);
        this.router.navigate(['/board']);
      },
      error: () => {
        this.hasError = true;
        this.errorMessage = "Les informations saisies sont invalides";
        this.isLoginFailed.emit(true);
        this.loading = false;
      }
    });

  }


}
