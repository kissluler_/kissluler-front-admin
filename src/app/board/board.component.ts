import { Component } from '@angular/core';
import { Router } from '@angular/router'; 
import { AuthService } from '../core/services/auth.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent {

  public isMenuOpen: boolean = true;

  title = 'kissluler-front-admin';

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  logout() {
    this.authService.logout().pipe().subscribe({
      complete: () => this.router.navigate(["/login"])
    });
  }

  public onSidenavClick(): void {
    this.isMenuOpen = false;
  }
}
